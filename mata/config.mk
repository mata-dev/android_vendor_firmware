FIRMWARE_IMAGES := \
    abl \
    bluetooth \
    cmnlib \
    cmnlib64 \
    dsp \
    modem \
    nvdef \
    tz

AB_OTA_PARTITIONS += $(FIRMWARE_IMAGES)
